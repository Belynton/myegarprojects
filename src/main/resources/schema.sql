create table if not exists employee
(
    id             bigserial    not null,
    grade          smallint     not null check (grade between 0 and 2),
    work_normative smallint     not null,
    first_name     varchar(255) not null,
    last_name      varchar(255) not null,
    primary key (id)
);
create table if not exists task
(
    id          bigserial    not null,
    difficulty  smallint     not null check (difficulty between 0 and 2),
    importance  smallint     not null check (importance between 0 and 1),
    finish_time timestamp(6),
    start_time  timestamp(6) not null,
    description varchar(255) not null,
    primary key (id)
);
create table if not exists employee_task
(
    employee_id bigint not null,
    task_id     bigint not null,
    primary key (employee_id, task_id)
);
create table if not exists late
(
    id          bigserial not null,
    date        date      not null,
    duration    smallint  not null,
    employee_id bigint    not null,
    primary key (id)
);
create table if not exists offday
(
    id          bigserial not null,
    date        date      not null,
    type        smallint     not null check (type between 0 and 2),
    is_payable  boolean   not null,
    employee_id bigint    not null,
    reason      varchar(255),
    primary key (id)
);
create table if not exists overwork
(
    id          bigserial not null,
    date        date      not null,
    duration    smallint  not null,
    employee_id bigint    not null,
    primary key (id)
);
create table if not exists task_daily_card
(
    id          bigserial   not null,
    date        date     not null,
    spent_time  smallint not null,
    employee_id bigint   not null,
    task_id     bigint   not null,
    primary key (id)
);

alter table employee_task drop constraint if exists FK_employee_task_task_id;
alter table employee_task drop constraint if exists FK_employee_task_employee_id;
alter table late drop constraint if exists FK_late_employee_id;
alter table offday drop constraint if exists FK_offday_employee_id;
alter table overwork drop constraint if exists FK_overwork_employee_id;
alter table task_daily_card drop constraint if exists FK_task_daily_card_employee_id;
alter table task_daily_card drop constraint if exists FK_task_daily_card_task_id;

alter table if exists employee_task add constraint FK_employee_task_task_id foreign key (task_id) references task;
alter table if exists employee_task add constraint FK_employee_task_employee_id foreign key (employee_id) references employee;
alter table if exists late add constraint FK_late_employee_id foreign key (employee_id) references employee;
alter table if exists offday add constraint FK_offday_employee_id foreign key (employee_id) references employee;
alter table if exists overwork add constraint FK_overwork_employee_id foreign key (employee_id) references employee;
alter table if exists task_daily_card add constraint FK_task_daily_card_employee_id foreign key (employee_id) references employee;
alter table if exists task_daily_card add constraint FK_task_daily_card_task_id foreign key (task_id) references task;