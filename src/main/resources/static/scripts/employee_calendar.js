const DAYS_OF_WEEK = [
    'Пн',
    'Вт',
    'Ср',
    'Чт',
    'Пт',
    'Сб',
    'Вс'
];

const MONTHS = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
];

let Cal = function (divId) {
    //Сохраняем идентификатор div
    this.divId = divId;
    //Устанавливаем текущий месяц, год
    let d = new Date();
    this.currMonth = d.getMonth();
    this.currYear = d.getFullYear();
    this.currDay = d.getDate();
    this.employeeData = "";
    this.dayInfoMap = {};
};
// Переход к следующему месяцу
Cal.prototype.nextMonth = function () {
    this.currMonth++;
    if (this.currMonth >= 12) {
        this.currMonth = 0;
        this.currYear++;
    }
    this.showCurrMonth(this.currYear, this.currMonth);
};
// Переход к предыдущему месяцу
Cal.prototype.previousMonth = function () {
    this.currMonth--;
    if (this.currMonth <= -1) {
        this.currMonth = 11;
        this.currYear--;
    }
    this.showCurrMonth(this.currYear, this.currMonth);
};
// Показать месяц (год, месяц)
Cal.prototype.showCurrMonth = function (year, month) {
    this.dayInfoMap = {}
    let firstDayOfMonth = new Date(year, month, 7).getDay()
    let lastDateOfMonth = new Date(year, month + 1, 0).getDate()
    let lastDayOfLastMonth = month === 0
        ? new Date(year - 1, 11, 0).getDate() : new Date(year, month, 0).getDate()
    onMonthChange(this)
    let html = '<table>';
    // Запись выбранного месяца и года
    html += '<thead>'
        + '<tr>'
        + '<td colspan="7">' + MONTHS[month] + ' ' + year + '</td>'
        + '</tr>'
        + '</thead>';
    // заголовок дней недели
    html += '<tr class="days">';
    for (let i = 0; i < DAYS_OF_WEEK.length; i++) {
        html += '<td>' + DAYS_OF_WEEK[i] + '</td>';
    }
    html += '</tr>';
    // Записываем дни
    let i = 1;
    do {
        let dow = new Date(year, month, i).getDay();
        // Начать новую строку в понедельник
        if (dow === 1) {
            html += '<tr>';
        }
        // Если первый день недели не понедельник показать последние дни предыдущего месяца
        else if (i === 1) {
            html += '<tr>';
            let k = lastDayOfLastMonth - firstDayOfMonth + 1;
            for (let j = 0; j < firstDayOfMonth; j++) {
                html += '<td class="not-current">' + k + '</td>';
                k++;
            }
        }
        // Записываем текущий день в цикл
        let chk = new Date();
        let chkY = chk.getFullYear();
        let chkM = chk.getMonth();
        if (chkY === this.currYear && chkM === this.currMonth && i === this.currDay) {
            html += '<td class="today myday" id="day-' + i + '">' + i + '</td>';
        } else {
            html += '<td class="normal myday" id="day-' + i + '">' + i + '</td>';
        }
        this.dayInfoMap["day-" + i] = {}
        // закрыть строку в воскресенье
        if (dow === 0) {
            html += '</tr>';
        }
        // Если последний день месяца не воскресенье, показать первые дни следующего месяца
        else if (i === lastDateOfMonth) {
            let k = 1;
            for (dow; dow < 7; dow++) {
                html += '<td class="not-current">' + k + '</td>';
                k++;
            }
        }
        i++;
    } while (i <= lastDateOfMonth);
    // Конец таблицы
    html += '</table>';
    // Записываем HTML в div
    // $(this.divId).html(html)
    document.getElementById(this.divId).innerHTML = html;
};
// При загрузке окна
window.onload = function () {
    // Начать календарь
    let c = new Cal("divCal");
    c.showCurrMonth(c.currYear, c.currMonth);
    // Привязываем кнопки «Следующий» и «Предыдущий»
    $("#btnNext").click(function () {
        c.nextMonth();
        addOnclickAction(c)
    });
    $("#btnPrev").click(function () {
        c.previousMonth();
        addOnclickAction(c)
    });
    addOnclickAction(c)
}

function addOnclickAction(c) {
    $(".myday").click(function () {
        let id = $(this).attr("id");
        $("#dayInfoContent").empty().append("<h5 class=\"card-title\"> Подробная информация о выбранном дне </h5>")
        if (c.dayInfoMap[id] === {}) {
            return
        }
        if (c.dayInfoMap[id]['late']) {
            $("#dayInfoContent").append("<p>Опаздание: " + c.dayInfoMap[id]['late']['duration'] + " минут</p>")
        }
        if (c.dayInfoMap[id]['overwork']) {
            $("#dayInfoContent").append("<p>Переработка: " + c.dayInfoMap[id]['overwork']['duration'] + " минут</p>")
        }
        if (c.dayInfoMap[id]['offday']) {
            $("#dayInfoContent").append("<p>Пропущенный день. Причина: " + c.dayInfoMap[id]['offday']['reason'] + "</p>")
        }
        if (c.dayInfoMap[id]['card']) {
            let sumTime = 0
            for (const cardInfo of c.dayInfoMap[id]['card']) {
                sumTime += cardInfo.spentTime
                $("#dayInfoContent").append("<p>Выполнял <a href='http://localhost:8080/task/" + cardInfo.task.id + "'>"
                    + cardInfo.task.id + "</a> задачу на протяжении " + cardInfo.spentTime + " часов </p>")
            }
            $("#dayInfoContent").append("<p>За день было отработано " + sumTime + " часов</p>")
        }
    });
}

// При переходе на другой месяц получаем данные по сотруднику, которые необходимо отобразить на календаре
function onMonthChange(calendar) {
    let emplId = window.location.href.split('/').pop().split('?').shift();
    getEmployeeInfo(emplId, calendar)
    getCardInfo(emplId, calendar)
    getSumWorkTimeForEmployee(emplId, calendar)
}


function getSumWorkTimeForEmployee(emplId, calendar) {
    $.ajax({
        url: 'http://localhost:8080/api/v1/task_daily_card/sum-by-employee',
        type: 'GET',
        data: {
            employeeId: emplId,
            monthNumber: calendar.currMonth + 1
        },
        dataType: 'text',
        success: function (data) {
            $("#sum-worktime").empty().append("За " + calendar.currYear + "-" + (calendar.currMonth + 1) + " отработано: " + (!data?'0':data) + " часов")
        }
    })
}

function getEmployeeInfo(emplId, calendar) {
    $.ajax({
        url: 'http://localhost:8080/api/v1/employee/with-month/' + emplId,
        type: 'GET',
        data: {
            monthNumber: calendar.currMonth + 1
        },
        dataType: 'json',
        success: function (data) {
            this.employeeData = data
            processEmployeeResponse(this.employeeData, calendar)
        },
        error: function () {
            console.log("Данные о сутруднике " + emplId + " за " + (calendar.currMonth + 1) + " месяц не найдены")
        }
    })
}

function getCardInfo(emplId, calendar) {
    $.ajax({
        url: 'http://localhost:8080/api/v1/task_daily_card/by-employee/' + emplId,
        type: 'GET',
        data: {
            monthNumber: calendar.currMonth + 1
        },
        dataType: 'json',
        success: function (data) {
            processCardResponse(data, calendar)
        },
        error: function () {
            console.log("Данные о сутруднике " + emplId + " за " + (calendar.currMonth + 1) + " месяц не найдены")
        }
    })
}

function processEmployeeResponse(response, calendar) {
    for (let offday of response['offdays']) {
        let day = new Date(offday['date']).getDate()
        switch (offday['type']) {
            case "SKIP":
                $("#day-" + day).addClass('skipped');
                break
            case "HOSPITALS":
                $("#day-" + day).addClass('hospital');
                break
            case "VACATION":
                $("#day-" + day).addClass('vacation');
                break
        }
        calendar.dayInfoMap["day-" + day]["offday"] = offday;
    }
    for (let overwork of response['overworks']) {
        let day = new Date(overwork['date']).getDate()
        $("#day-" + day).addClass('overworked');
        calendar.dayInfoMap['day-' + day]['overwork'] = overwork;
    }
    for (let late of response['late']) {
        let day = new Date(late['date']).getDate()
        $("#day-" + day).addClass('late');
        calendar.dayInfoMap["day-" + day]["late"] = late;
    }
}

function processCardResponse(response, calendar) {

    for (const card of response) {
        const day = new Date(card['date']).getDate()
        $("#day-" + day).addClass('processed');
        if (!calendar.dayInfoMap["day-" + day]["card"]) {
            calendar.dayInfoMap["day-" + day]["card"] = [card]
        } else {
            const currentData = calendar.dayInfoMap["day-" + day]["card"]
            calendar.dayInfoMap["day-" + day]["card"] = [...currentData, card]
        }
    }
}