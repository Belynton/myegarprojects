const DAYS_OF_WEEK = [
    'Пн',
    'Вт',
    'Ср',
    'Чт',
    'Пт',
    'Сб',
    'Вс'
];

const MONTHS = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
];

let Cal = function (divId) {
    //Сохраняем идентификатор div
    this.divId = divId;
    //Устанавливаем текущий месяц, год
    let d = new Date();
    this.currMonth = d.getMonth();
    this.currYear = d.getFullYear();
    this.currDay = d.getDate();
    this.taskData = "";
    this.dayInfoMap = {};
};
// Переход к следующему месяцу
Cal.prototype.nextMonth = function () {
    this.currMonth++;
    if (this.currMonth >= 12) {
        this.currMonth = 0;
        this.currYear++;
    }
    this.showCurrMonth(this.currYear, this.currMonth);
};
// Переход к предыдущему месяцу
Cal.prototype.previousMonth = function () {
    this.currMonth--;
    if (this.currMonth <= -1) {
        this.currMonth = 11;
        this.currYear--;
    }
    this.showCurrMonth(this.currYear, this.currMonth);
};
// Показать месяц (год, месяц)
Cal.prototype.showCurrMonth = function (year, month) {
    this.dayInfoMap = {}
    let firstDayOfMonth = new Date(year, month, 7).getDay()
    let lastDateOfMonth = new Date(year, month + 1, 0).getDate()
    let lastDayOfLastMonth = month === 0
        ? new Date(year - 1, 11, 0).getDate() : new Date(year, month, 0).getDate()
    onMonthChange(this)
    let html = '<table>';
    // Запись выбранного месяца и года
    html += '<thead>'
        + '<tr>'
        + '<td colspan="7">' + MONTHS[month] + ' ' + year + '</td>'
        + '</tr>'
        + '</thead>';
    // заголовок дней недели
    html += '<tr class="days">';
    for (let i = 0; i < DAYS_OF_WEEK.length; i++) {
        html += '<td>' + DAYS_OF_WEEK[i] + '</td>';
    }
    html += '</tr>';
    // Записываем дни
    let i = 1;
    do {
        let dow = new Date(year, month, i).getDay();
        // Начать новую строку в понедельник
        if (dow === 1) {
            html += '<tr>';
        }
        // Если первый день недели не понедельник показать последние дни предыдущего месяца
        else if (i === 1) {
            html += '<tr>';
            let k = lastDayOfLastMonth - firstDayOfMonth + 1;
            for (let j = 0; j < firstDayOfMonth; j++) {
                html += '<td class="not-current">' + k + '</td>';
                k++;
            }
        }
        // Записываем текущий день в цикл
        let chk = new Date();
        let chkY = chk.getFullYear();
        let chkM = chk.getMonth();
        if (chkY === this.currYear && chkM === this.currMonth && i === this.currDay) {
            html += '<td class="today myday" id="day-' + i + '">' + i + '</td>';
        } else {
            html += '<td class="normal myday" id="day-' + i + '">' + i + '</td>';
        }
        // закрыть строку в воскресенье
        if (dow === 0) {
            html += '</tr>';
        }
        // Если последний день месяца не воскресенье, показать первые дни следующего месяца
        else if (i === lastDateOfMonth) {
            let k = 1;
            for (dow; dow < 7; dow++) {
                html += '<td class="not-current">' + k + '</td>';
                k++;
            }
        }
        i++;
    } while (i <= lastDateOfMonth);
    // Конец таблицы
    html += '</table>';
    // Записываем HTML в div
    document.getElementById(this.divId).innerHTML = html;
};
// При загрузке окна
window.onload = function () {
    // Начать календарь
    let c = new Cal("divCal");
    c.showCurrMonth(c.currYear, c.currMonth);
    // Привязываем кнопки «Следующий» и «Предыдущий»
    $("#btnNext").click(function () {
        c.nextMonth();
        addOnclickAction(c)
    });
    $("#btnPrev").click(function () {
        c.previousMonth();
        addOnclickAction(c)
    });
    addOnclickAction(c)
}

function addOnclickAction(c) {
    $(".myday").click(function () {
        let id = $(this).attr("id");
        $("#dayInfoContent").empty().append("<h5 class=\"card-title\"> Подробная информация о затраченном времени на задачу </h5>")
        let sumTime = 0
        for (const cardInfo of c.dayInfoMap[id]) {
            sumTime += cardInfo.spentTime
            let empFio = cardInfo.employee.firstName + " " + cardInfo.employee.lastName
            $("#dayInfoContent").append("<p>Сотрудник <a href='http://localhost:8080/employee/"
                + cardInfo.employee.id + "'>" + empFio + "</a> затратил "
                + cardInfo.spentTime + " часов на эту задачу</p>")
        }
        $("#dayInfoContent").append("В выбранный день на задачу затрачено " + sumTime + " часов")
    });
}

// При переходе на другой месяц получаем данные по работе над задачей, которую необходимо отобразить на календаре
function onMonthChange(calendar) {
    let taskId = window.location.href.split('/').pop().split('?').shift();
    $.ajax({
        url: 'http://localhost:8080/api/v1/task_daily_card/by-task/' + taskId,
        type: 'GET',
        data: {
            monthNumber: calendar.currMonth + 1
        },
        dataType: 'json',
        success: function (data) {
            this.taskData = data
            processTaskCardResponse(this.taskData, calendar)
        },
        error: function () {
            console.log("Данные о работе над задачей " + taskId + " за " + (calendar.currMonth + 1) + " месяц не найдены")
        }
    })
}

function processTaskCardResponse(response, calendar) {
    for (const card of response) {
        const startDate = new Date(card["task"]["startTime"]).toDateString()
        const finishDate = new Date(card["task"]["finishTime"]).toDateString()
        const date = new Date(card['date'])
        const day = date.getDate()

        switch (date.toDateString()) {
            case startDate:
                $("#day-" + day).addClass('task-started');
                break
            case finishDate:
                $("#day-" + day).addClass('task-finished');
                break
            default:
                $("#day-" + day).addClass('processed');
        }

        if (!calendar.dayInfoMap["day-" + day]) {
            calendar.dayInfoMap["day-" + day] = [card]
        } else {
            const currentData = calendar.dayInfoMap["day-" + day]
            calendar.dayInfoMap["day-" + day] = [...currentData, card]
        }
    }
}