package ru.egar.exception;

import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
public class ExceptionMessage {
    private final LocalDateTime errorMessageTime;
    private final String message;
}
