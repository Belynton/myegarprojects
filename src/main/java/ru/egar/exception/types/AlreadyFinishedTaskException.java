package ru.egar.exception.types;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class AlreadyFinishedTaskException extends RuntimeException {
    public AlreadyFinishedTaskException(String message) {
        super(message);
    }
}
