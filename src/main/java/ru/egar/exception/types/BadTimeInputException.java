package ru.egar.exception.types;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class BadTimeInputException extends RuntimeException {
    public BadTimeInputException(String message) {
        super(message);
    }
}
