package ru.egar.configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(title = "Учет рабочего времени сотрудника")
)
public class OpenApiConfig {
}
