package ru.egar.domain.employee.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.egar.domain.employee.dto.EmployeeFullResponseDto;
import ru.egar.domain.employee.dto.EmployeeShortResponseDto;
import ru.egar.domain.employee.dto.SaveEmployeeDto;
import ru.egar.domain.employee.entity.Employee;
import ru.egar.domain.employee.mapper.EmployeeMapper;
import ru.egar.domain.employee.repository.EmployeeRepository;
import ru.egar.exception.types.ElementNotFoundException;
import ru.egar.utils.NullAwareBeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final String NOT_FOUND_MESSAGE = "Сотрудник с id = '%s' не найден";

    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;

    public EmployeeFullResponseDto getById(Long id) {
        var employee = findById(id);
        return employeeMapper.toFullEmployee(employee);
    }

    public EmployeeFullResponseDto getByIdAndMonthNumber(Long id, Long monthNumber) {
        var employee = findById(id);
        employee.setTasks(employee.getTasks().stream()
                .filter(task -> task.getStartTime().getMonthValue() == monthNumber)
                .collect(Collectors.toSet()));
        employee.setOffdays(employee.getOffdays().stream()
                .filter(offday -> offday.getDate().getMonthValue() == monthNumber)
                .collect(Collectors.toSet()));
        employee.setOverworks(employee.getOverworks().stream()
                .filter(overwork -> overwork.getDate().getMonthValue() == monthNumber)
                .collect(Collectors.toSet()));
        employee.setLate(employee.getLate().stream()
                .filter(late -> late.getDate().getMonthValue() == monthNumber)
                .collect(Collectors.toSet()));
        return employeeMapper.toFullEmployee(employee);
    }

    public List<EmployeeShortResponseDto> getAll() {
        return employeeRepository.findAll()
                .stream()
                .map(employeeMapper::toShortEmployee)
                .toList();
    }

    @Transactional
    public Long create(SaveEmployeeDto saveEmployeeDto) {
        var employeeToSave = employeeMapper.toEmployee(saveEmployeeDto);
        var savedEmployee = employeeRepository.save(employeeToSave);
        return savedEmployee.getId();
    }

    @Transactional
    public Long update(Long id, SaveEmployeeDto saveEmployeeDto) {
        var employee = findById(id);
        NullAwareBeanUtils.copyNonNullProperties(employeeMapper.toEmployee(saveEmployeeDto), employee);
        var savedEmployee = employeeRepository.save(employee);
        return savedEmployee.getId();
    }

    @Transactional
    public void delete(Long id) {
        var employee = findById(id);
        employeeRepository.delete(employee);
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new ElementNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));
    }
}