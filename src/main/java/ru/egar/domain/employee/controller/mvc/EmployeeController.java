package ru.egar.domain.employee.controller.mvc;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.egar.domain.card.service.TaskDailyCardService;
import ru.egar.domain.employee.dto.SaveEmployeeDto;
import ru.egar.domain.employee.enums.Grade;
import ru.egar.domain.employee.service.EmployeeService;
import ru.egar.domain.late.dto.SaveLateDto;
import ru.egar.domain.offday.dto.SaveOffdayDto;
import ru.egar.domain.offday.enums.OffdayType;
import ru.egar.domain.overwork.dto.SaveOverworkDto;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;
    private final TaskDailyCardService cardService;

    @GetMapping("/")
    public String main() {
        return "redirect:/employee";
    }

    @GetMapping("/employee")
    public String getAllEmployee(Model model) {
        model.addAttribute("employeeList", employeeService.getAll());
        model.addAttribute("gradeVariants", Grade.values());
        model.addAttribute("saveEmployeeDto", new SaveEmployeeDto());
        return "index";
    }

    @PostMapping("/employee")
    public String createEmployee(@Valid @ModelAttribute SaveEmployeeDto saveEmployeeDto) {
        employeeService.create(saveEmployeeDto);
        return "redirect:/";
    }

    @GetMapping("/employee/{id}")
    public String getEmployeeById(@PathVariable Long id, Model model) {
        var currentDate = LocalDate.now();
        model.addAttribute("selectedEmployee", employeeService.getById(id));
        model.addAttribute("currDate", currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM")));
        model.addAttribute("sumByMonth", cardService.getSummaryForEmployee(id, (long) currentDate.getMonthValue()));
        model.addAttribute("typeVariants", OffdayType.values());
        model.addAttribute("saveLateDto", new SaveLateDto());
        model.addAttribute("saveOffdayDto", new SaveOffdayDto());
        model.addAttribute("saveOverworkDto", new SaveOverworkDto());
        return "selectedEmployee";
    }
}
