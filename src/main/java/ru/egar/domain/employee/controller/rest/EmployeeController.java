package ru.egar.domain.employee.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.egar.domain.employee.dto.EmployeeFullResponseDto;
import ru.egar.domain.employee.dto.EmployeeShortResponseDto;
import ru.egar.domain.employee.dto.SaveEmployeeDto;

import java.util.List;

@RequestMapping("api/v1/employee")
public interface EmployeeController {

    @Operation(
            summary = "Получить список всех сотрудников",
            description = "Возвращает список всех сотрудников",
            method = "GET"
    )
    @GetMapping
    ResponseEntity<List<EmployeeShortResponseDto>> getEmployees();

    @Operation(
            summary = "Получить сотрудника по идентификатору",
            description = "Возвращает сотрудника с указанным идентификатором",
            method = "GET"
    )
    @GetMapping("/{id}")
    ResponseEntity<EmployeeFullResponseDto> getEmployeeById(
            @PathVariable @Parameter(description = "Идентификатор сотрудника") Long id
    );

    @Operation(
            summary = "Получить иформацию о сотрудника за указанный месяц",
            description = "Возвращает сотрудника с указанным идентификатором, с информацией за указанный месяц",
            method = "GET"
    )
    @GetMapping("/with-month/{id}")
    ResponseEntity<EmployeeFullResponseDto> getEmployeeByIdAndMonthNumber(
            @PathVariable @Parameter(description = "Идентификатор сотрудника") Long id,
            @RequestParam @Parameter(description = "Номер месяца") Long monthNumber
    );

    @Operation(
            summary = "Добавить нового сотрудника",
            description = "Добавляет нового сотрудника",
            method = "POST"
    )
    @PostMapping
    ResponseEntity<Long> createEmployee(
            @Valid @RequestBody @Parameter(description = "Сотрудник, который будет добавлен") SaveEmployeeDto employee
    );

    @Operation(
            summary = "Изменить сотрудника",
            description = "Изменить данные сотрудника по его id",
            method = "POST"
    )
    @PatchMapping("/{id}")
    ResponseEntity<Long> updateEmployee(
            @PathVariable @Parameter(description = "Идентификатор сотрудника") Long id,
            @Valid @RequestBody @Parameter(description = "Данные, которые необходимо заменить") SaveEmployeeDto employee
    );

    @Operation(
            summary = "Удалить сотрудника",
            description = "Удалить сотрудника по его id",
            method = "DELETE"
    )
    @DeleteMapping("/{id}")
    ResponseEntity<Long> deleteEmployee(
            @PathVariable @Parameter(description = "Идентификатор сотрудника") Long id
    );

}
