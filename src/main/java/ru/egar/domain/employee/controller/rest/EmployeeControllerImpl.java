package ru.egar.domain.employee.controller.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.egar.domain.employee.dto.EmployeeFullResponseDto;
import ru.egar.domain.employee.dto.EmployeeShortResponseDto;
import ru.egar.domain.employee.dto.SaveEmployeeDto;
import ru.egar.domain.employee.service.EmployeeService;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class EmployeeControllerImpl implements EmployeeController {

    private final EmployeeService employeeService;

    @Override
    public ResponseEntity<List<EmployeeShortResponseDto>> getEmployees() {
        return new ResponseEntity<>(employeeService.getAll(), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<EmployeeFullResponseDto> getEmployeeById(Long id) {
        return new ResponseEntity<>(employeeService.getById(id), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<EmployeeFullResponseDto> getEmployeeByIdAndMonthNumber(Long id, Long monthNumber) {
        return new ResponseEntity<>(employeeService.getByIdAndMonthNumber(id, monthNumber), HttpStatusCode.valueOf(200));

    }

    @Override
    public ResponseEntity<Long> createEmployee(SaveEmployeeDto employee) {
        return new ResponseEntity<>(employeeService.create(employee), HttpStatusCode.valueOf(202));
    }

    @Override
    public ResponseEntity<Long> updateEmployee(Long id, SaveEmployeeDto employee) {
        return new ResponseEntity<>(employeeService.update(id, employee), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> deleteEmployee(Long id) {
        employeeService.delete(id);
        return new ResponseEntity<>(id, HttpStatusCode.valueOf(200));
    }
}
