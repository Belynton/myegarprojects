package ru.egar.domain.employee.enums;

public enum Grade {
    JUNIOR, MIDDLE, SENIOR
}
