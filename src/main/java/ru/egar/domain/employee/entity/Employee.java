package ru.egar.domain.employee.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.egar.domain.employee.enums.Grade;
import ru.egar.domain.late.entity.Late;
import ru.egar.domain.offday.entity.Offday;
import ru.egar.domain.overwork.entity.Overwork;
import ru.egar.domain.task.entity.Task;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Сущность сотрудника.
 * Включает в себя: фамилию, имя, грейд, недельный норматив(в часах).
 * Связана с задачами сотрудника, выходными, переработками.
 */
@Getter
@Setter
@Entity
@Table(name = "employee")
@RequiredArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private Grade grade;
    @Column(nullable = false)
    private Short workNormative;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "employee_task",
            joinColumns = {@JoinColumn(name = "employee_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "task_id", referencedColumnName = "id")}
    )
    private Set<Task> tasks = new HashSet<>();
    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
    private Set<Offday> offdays = new HashSet<>();
    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
    private Set<Overwork> overworks = new HashSet<>();
    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER)
    private Set<Late> late = new HashSet<>();

    public void addTask(Task task) {
        this.tasks.add(task);
        task.getEmployees().add(this);
    }

    public void removeTask(Task task) {
        this.tasks.remove(task);
        task.getEmployees().remove(this);
    }

    public boolean isOffdayAlreadyCreated(LocalDate newOffdayDate) {
        return offdays.stream().anyMatch(offday -> offday.getDate().isEqual(newOffdayDate));
    }

    public boolean isOverworkAlreadyCreated(LocalDate newOverworkDate) {
        return overworks.stream().anyMatch(overwork -> overwork.getDate().isEqual(newOverworkDate));
    }

    public boolean isLateAlreadyCreatedAtDate(LocalDate newLateDate) {
        return late.stream().anyMatch(late -> late.getDate().isEqual(newLateDate));
    }
}
