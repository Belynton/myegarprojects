package ru.egar.domain.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.egar.domain.employee.entity.Employee;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("""
            SELECT e FROM Employee e
            LEFT JOIN FETCH e.tasks t
            LEFT JOIN FETCH e.overworks ov
            LEFT JOIN FETCH e.offdays off
            LEFT JOIN FETCH e.late l
            WHERE e.id = :id
            AND coalesce((MONTH(t.startTime) = :monthNumber), true)
            AND coalesce((MONTH(ov.date) = :monthNumber), true)
            AND coalesce((MONTH(off.date) = :monthNumber), true)
            AND coalesce((MONTH(l.date) = :monthNumber), true)
            """
    )
    Optional<Employee> getEmployeeInfoByMonth(@Param("id") Long id, @Param("monthNumber") Long monthNumber);

}
