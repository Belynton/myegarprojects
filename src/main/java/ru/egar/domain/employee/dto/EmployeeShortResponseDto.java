package ru.egar.domain.employee.dto;

import lombok.Data;
import ru.egar.domain.employee.enums.Grade;

@Data
public class EmployeeShortResponseDto {
    private Long id;
    private String firstName;
    private String lastName;
    private Grade grade;
}
