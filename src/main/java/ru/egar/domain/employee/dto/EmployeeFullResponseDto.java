package ru.egar.domain.employee.dto;

import lombok.Data;
import ru.egar.domain.employee.enums.Grade;
import ru.egar.domain.late.dto.LateShortResponseDto;
import ru.egar.domain.offday.dto.OffdayShortResponseDto;
import ru.egar.domain.overwork.dto.OverworkShortResponseDto;
import ru.egar.domain.task.dto.TaskShortResponseDto;

import java.util.Set;

@Data
public class EmployeeFullResponseDto {
    private Long id;
    private String firstName;
    private String lastName;
    private Grade grade;
    private Short workNormative;
    private Set<TaskShortResponseDto> tasks;
    private Set<OffdayShortResponseDto> offdays;
    private Set<OverworkShortResponseDto> overworks;
    private Set<LateShortResponseDto> late;
}
