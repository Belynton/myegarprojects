package ru.egar.domain.employee.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import ru.egar.domain.employee.enums.Grade;


@Data
public class SaveEmployeeDto {
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @Min(1)
    @Max(168)
    private Short workNormative;
    private Grade grade;
}
