package ru.egar.domain.employee.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import ru.egar.domain.employee.dto.EmployeeFullResponseDto;
import ru.egar.domain.employee.dto.EmployeeShortResponseDto;
import ru.egar.domain.employee.dto.SaveEmployeeDto;
import ru.egar.domain.employee.entity.Employee;
import ru.egar.domain.late.mapper.LateMapper;
import ru.egar.domain.offday.mapper.OffdayMapper;
import ru.egar.domain.overwork.mapper.OverworkMapper;
import ru.egar.domain.task.mapper.TaskMapper;

@Mapper(componentModel = "spring",
        uses = {TaskMapper.class,
                OffdayMapper.class,
                OverworkMapper.class,
                LateMapper.class},
        injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface EmployeeMapper {

    EmployeeFullResponseDto toFullEmployee(Employee employee);

    EmployeeShortResponseDto toShortEmployee(Employee employee);

    Employee toEmployee(SaveEmployeeDto saveEmployeeDto);
}
