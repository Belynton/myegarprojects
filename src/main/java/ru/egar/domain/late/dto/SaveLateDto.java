package ru.egar.domain.late.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.PastOrPresent;
import lombok.Data;

import java.time.LocalDate;

@Data
public class SaveLateDto {
    @PastOrPresent(message = "Дата опаздания не может быть позже текущего дня")
    private LocalDate date;
    @Min(1)
    @Max(1440)
    private Short duration;
    private Long employeeId;
}
