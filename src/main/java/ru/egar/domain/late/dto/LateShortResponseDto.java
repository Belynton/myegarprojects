package ru.egar.domain.late.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class LateShortResponseDto {
    private Long id;
    private LocalDate date;
    private Short duration;
}
