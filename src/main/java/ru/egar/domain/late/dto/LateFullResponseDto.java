package ru.egar.domain.late.dto;

import lombok.Data;
import ru.egar.domain.employee.dto.EmployeeShortResponseDto;

import java.time.LocalDate;

@Data
public class LateFullResponseDto {
    private Long id;
    private LocalDate date;
    private Short duration;
    private EmployeeShortResponseDto employee;
}
