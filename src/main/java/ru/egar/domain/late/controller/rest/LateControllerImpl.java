package ru.egar.domain.late.controller.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.egar.domain.late.dto.LateFullResponseDto;
import ru.egar.domain.late.dto.LateShortResponseDto;
import ru.egar.domain.late.dto.SaveLateDto;
import ru.egar.domain.late.service.LateService;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class LateControllerImpl implements LateController {

    private final LateService lateService;

    @Override
    public ResponseEntity<List<LateShortResponseDto>> getLate() {
        return new ResponseEntity<>(lateService.getAll(), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<LateFullResponseDto> getLateById(Long id) {
        return new ResponseEntity<>(lateService.getById(id), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> createLate(SaveLateDto saveLateDto) {
        return new ResponseEntity<>(lateService.create(saveLateDto), HttpStatusCode.valueOf(202));
    }

    @Override
    public ResponseEntity<Long> updateLate(Long id, SaveLateDto saveLateDto) {
        return new ResponseEntity<>(lateService.update(id, saveLateDto), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> deleteLate(Long id) {
        lateService.delete(id);
        return new ResponseEntity<>(id, HttpStatusCode.valueOf(200));
    }
}
