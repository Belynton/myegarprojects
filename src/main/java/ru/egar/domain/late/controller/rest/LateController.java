package ru.egar.domain.late.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.egar.domain.late.dto.LateFullResponseDto;
import ru.egar.domain.late.dto.LateShortResponseDto;
import ru.egar.domain.late.dto.SaveLateDto;

import java.util.List;

@RequestMapping("api/v1/late")
public interface LateController {

    @Operation(
            summary = "Получить список всех опазданий",
            description = "Возвращает список всех опазданий",
            method = "GET"
    )
    @GetMapping
    ResponseEntity<List<LateShortResponseDto>> getLate();

    @Operation(
            summary = "Получить опаздание по идентификатору",
            description = "Возвращает опаздание с указанным идентификатором",
            method = "GET"
    )
    @GetMapping("/{id}")
    ResponseEntity<LateFullResponseDto> getLateById(
            @PathVariable @Parameter(description = "Идентификатор опаздания") Long id
    );

    @Operation(
            summary = "Добавить опаздание",
            description = "Добавляет опаздание для сотрудника",
            method = "POST"
    )
    @PostMapping
    ResponseEntity<Long> createLate(
            @Valid @RequestBody @Parameter(description = "Опаздание, которое будет добавлено") SaveLateDto saveLateDto
    );

    @Operation(
            summary = "Изменить опаздание",
            description = "Изменить данные об опаздании по его id",
            method = "POST"
    )
    @PatchMapping("/{id}")
    ResponseEntity<Long> updateLate(
            @PathVariable @Parameter(description = "Идентификатор опаздания") Long id,
            @Valid @RequestBody @Parameter(description = "Данные, которые необходимо заменить") SaveLateDto saveLateDto
    );

    @Operation(
            summary = "Удалить опаздание",
            description = "Удалить опаздание по его id",
            method = "DELETE"
    )
    @DeleteMapping("/{id}")
    ResponseEntity<Long> deleteLate(
            @PathVariable @Parameter(description = "Идентификатор опаздания") Long id
    );
}
