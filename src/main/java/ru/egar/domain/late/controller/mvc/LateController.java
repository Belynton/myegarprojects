package ru.egar.domain.late.controller.mvc;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.egar.domain.late.dto.SaveLateDto;
import ru.egar.domain.late.service.LateService;

@Controller
@RequiredArgsConstructor
public class LateController {

    private final LateService lateService;

    @PostMapping("/late")
    public String createLate(@Valid @ModelAttribute SaveLateDto saveLateDto) {
        var lateId = lateService.create(saveLateDto);
        var employeeId = lateService.findById(lateId).getEmployee().getId();
        return "redirect:/employee/" + employeeId;
    }
}
