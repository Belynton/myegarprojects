package ru.egar.domain.late.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.egar.domain.employee.entity.Employee;
import ru.egar.domain.employee.mapper.EmployeeMapper;
import ru.egar.domain.employee.service.EmployeeService;
import ru.egar.domain.late.dto.LateFullResponseDto;
import ru.egar.domain.late.dto.LateShortResponseDto;
import ru.egar.domain.late.dto.SaveLateDto;
import ru.egar.domain.late.entity.Late;
import ru.egar.domain.late.mapper.LateMapper;
import ru.egar.domain.late.repository.LateRepository;
import ru.egar.exception.types.BadTimeInputException;
import ru.egar.exception.types.ElementNotFoundException;
import ru.egar.utils.NullAwareBeanUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LateService {

    private final String NOT_FOUND_MESSAGE = "Опаздание с id = '%s' не найдено";

    private final LateRepository lateRepository;
    private final LateMapper lateMapper;
    private final EmployeeService employeeService;
    private final EmployeeMapper employeeMapper;

    public LateFullResponseDto getById(Long id) {
        var late = findById(id);
        var fullLate = lateMapper.toFullLate(late);
        fullLate.setEmployee(employeeMapper.toShortEmployee(late.getEmployee()));
        return fullLate;
    }

    public List<LateShortResponseDto> getAll() {
        return lateRepository.findAll()
                .stream()
                .map(lateMapper::toShortLate)
                .toList();
    }

    @Transactional
    public Long create(SaveLateDto saveLateDto) {
        var employee = employeeService.findById(saveLateDto.getEmployeeId());
        throwIfLateCantBeCreated(employee, saveLateDto.getDate());
        var lateToSave = lateMapper.toLate(saveLateDto);
        lateToSave.setEmployee(employee);
        var savedOffday = lateRepository.save(lateToSave);
        return savedOffday.getId();
    }

    @Transactional
    public Long update(Long id, SaveLateDto saveLateDto) {
        var late = findById(id);
        NullAwareBeanUtils.copyNonNullProperties(lateMapper.toLate(saveLateDto), late);
        Optional.ofNullable(saveLateDto.getEmployeeId())
                .ifPresent(dtoId -> late.setEmployee(employeeService.findById(dtoId)));
        var savedLate = lateRepository.save(late);
        return savedLate.getId();
    }

    @Transactional
    public void delete(Long id) {
        var late = findById(id);
        lateRepository.delete(late);
    }

    public Late findById(Long id) {
        return lateRepository.findById(id)
                .orElseThrow(() -> new ElementNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));
    }

    private void throwIfLateCantBeCreated(Employee employee, LocalDate saveOverworkDate) {
        if (employee.isOffdayAlreadyCreated(saveOverworkDate)) {
            throw new BadTimeInputException("Невозможно указать опаздание в пропущенный день");
        }
        if (employee.isLateAlreadyCreatedAtDate(saveOverworkDate)) {
            throw new BadTimeInputException("Опаздание на указанную дату уже существует");
        }
    }
}
