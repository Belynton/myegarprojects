package ru.egar.domain.late.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.egar.domain.late.dto.LateFullResponseDto;
import ru.egar.domain.late.dto.LateShortResponseDto;
import ru.egar.domain.late.dto.SaveLateDto;
import ru.egar.domain.late.entity.Late;

@Mapper(
        componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface LateMapper {
    @Mapping(target = "employee", ignore = true)
    LateFullResponseDto toFullLate(Late late);

    LateShortResponseDto toShortLate(Late late);

    Late toLate(SaveLateDto saveLateDto);

}
