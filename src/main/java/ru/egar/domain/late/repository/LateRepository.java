package ru.egar.domain.late.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.egar.domain.late.entity.Late;

public interface LateRepository extends JpaRepository<Late, Long> {
}
