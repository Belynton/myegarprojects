package ru.egar.domain.late.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.egar.domain.employee.entity.Employee;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "late")
@RequiredArgsConstructor
public class Late {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private LocalDate date;
    @Column(nullable = false)
    private Short duration;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;

    public void addEmployee(Employee employee) {
        this.employee = employee;
        employee.getLate().add(this);
    }

    public void removeEmployee(Employee employee) {
        this.employee = null;
        employee.getLate().remove(this);
    }
}
