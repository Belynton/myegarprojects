package ru.egar.domain.card.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.egar.domain.card.dto.SaveTaskDailyCardDto;
import ru.egar.domain.card.dto.TaskDailyCardResponseDto;
import ru.egar.domain.card.entity.TaskDailyCard;
import ru.egar.domain.card.mapper.TaskDailyCardMapper;
import ru.egar.domain.card.repository.TaskDailyCardRepository;
import ru.egar.domain.employee.entity.Employee;
import ru.egar.domain.employee.service.EmployeeService;
import ru.egar.domain.task.entity.Task;
import ru.egar.domain.task.service.TaskService;
import ru.egar.exception.types.BadTimeInputException;
import ru.egar.exception.types.ElementNotFoundException;
import ru.egar.utils.NullAwareBeanUtils;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskDailyCardService {

    private final String NOT_FOUND_MESSAGE = "Журнал о затраченном времени с id = '%s' не найден";

    private final TaskDailyCardRepository repository;
    private final TaskDailyCardMapper taskDailyCardMapper;
    private final EmployeeService employeeService;
    private final TaskService taskService;

    public List<TaskDailyCardResponseDto> getAllByTaskIdAndMonthNumber(Long taskId, Long monthNumber) {
        return repository.findAllByTaskIdAndMonthNumber(taskId, monthNumber)
                .stream()
                .map(taskDailyCardMapper::toTaskDailyCardResponseDto)
                .toList();

    }

    public List<TaskDailyCardResponseDto> getAllByEmployeeIdAndMonthNumber(Long employeeId, Long monthNumber) {
        return repository.findAllByEmployeeIdAndMonthNumber(employeeId, monthNumber)
                .stream()
                .map(taskDailyCardMapper::toTaskDailyCardResponseDto)
                .toList();

    }

    public Long getSummary(Long taskId) {
        return repository.getSummaryTaskDuration(taskId);
    }

    public Long getSummaryForEmployee(Long employeeId, Long monthNumber) {
        return repository.getSummaryWorkDurationForEmployeeByMonth(employeeId, monthNumber);
    }

    public List<TaskDailyCardResponseDto> getAll() {
        return repository.findAll()
                .stream()
                .map(taskDailyCardMapper::toTaskDailyCardResponseDto)
                .toList();
    }

    @Transactional
    public Long create(SaveTaskDailyCardDto saveTaskDailyCardDto) {
        var employee = employeeService.findById(saveTaskDailyCardDto.getEmployeeId());
        var task = taskService.findById(saveTaskDailyCardDto.getTaskId());
        throwIfBadCardDateInput(saveTaskDailyCardDto.getDate(), task);
        throwIfOffday(saveTaskDailyCardDto.getDate(), employee);
        var taskDailyCardToSave = taskDailyCardMapper.toTaskDailyCard(saveTaskDailyCardDto);
        taskDailyCardToSave.setEmployee(employee);
        taskDailyCardToSave.setTask(task);
        var savedTaskDailyCard = repository.save(taskDailyCardToSave);
        return savedTaskDailyCard.getId();
    }

    @Transactional
    public Long update(Long id, SaveTaskDailyCardDto saveTaskDailyCardDto) {
        var employee = employeeService.findById(saveTaskDailyCardDto.getEmployeeId());
        var task = taskService.findById(saveTaskDailyCardDto.getTaskId());
        var taskDailyCard = findById(id);
        throwIfBadCardDateInput(saveTaskDailyCardDto.getDate(), task);
        throwIfOffday(saveTaskDailyCardDto.getDate(), employee);
        NullAwareBeanUtils.copyNonNullProperties(taskDailyCardMapper.toTaskDailyCard(saveTaskDailyCardDto), taskDailyCard);
        taskDailyCard.setEmployee(employee);
        taskDailyCard.setTask(task);
        var savedTaskDailyCard = repository.save(taskDailyCard);
        return savedTaskDailyCard.getId();
    }

    @Transactional
    public void delete(Long id) {
        var taskDailyCard = findById(id);
        repository.delete(taskDailyCard);
    }

    public TaskDailyCard findById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new ElementNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));
    }

    private void throwIfBadCardDateInput(LocalDate inputDate, Task task) {
        if (task.getStartTime().toLocalDate().isAfter(inputDate)) {
            throw new BadTimeInputException(
                    String.format("Дата работы над задачей '%s' не может быть раньше даты начала выполнения задачи '%s'",
                            inputDate,
                            task.getStartTime().toLocalDate()));
        }
        if (task.getFinishTime() != null && task.getFinishTime().toLocalDate().isBefore(inputDate)) {
            throw new BadTimeInputException(
                    String.format("Дата работы над задачей '%s' не может быть после даты завершения задачи '%s'",
                            inputDate,
                            task.getStartTime().toLocalDate()));
        }
    }

    private void throwIfOffday(LocalDate inputDate, Employee employee) {
        if (employee.getOffdays().stream().anyMatch(offday -> offday.getDate().isEqual(inputDate))) {
            throw new BadTimeInputException(
                    String.format("Дата работы над задачей и время пропущенного дня '%s' не могут совпадать", inputDate));
        }
    }
}
