package ru.egar.domain.card.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.egar.domain.employee.entity.Employee;
import ru.egar.domain.task.entity.Task;

import java.time.LocalDate;


/**
 * Сущность затраченного на задачу времени.
 * Содержит в себе дату выполнения задачи, затраченное время(в часах), сотрудника, задачу
 */
@Getter
@Setter
@Entity
@Table(name = "task_daily_card")
@RequiredArgsConstructor
public class TaskDailyCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private LocalDate date;
    @Column(nullable = false)
    private Short spentTime;
    @OneToOne
    @JoinColumn(nullable = false)
    private Employee employee;
    @OneToOne
    @JoinColumn(nullable = false)
    private Task task;
}
