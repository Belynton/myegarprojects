package ru.egar.domain.card.dto;

import lombok.Data;
import ru.egar.domain.employee.dto.EmployeeShortResponseDto;
import ru.egar.domain.task.dto.TaskShortResponseDto;

import java.time.LocalDate;

@Data
public class TaskDailyCardResponseDto {
    private Long id;
    private LocalDate date;
    private Short spentTime;
    private EmployeeShortResponseDto employee;
    private TaskShortResponseDto task;
}
