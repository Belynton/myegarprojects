package ru.egar.domain.card.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.PastOrPresent;
import lombok.Data;

import java.time.LocalDate;

@Data
public class SaveTaskDailyCardDto {
    @PastOrPresent(message = "Дата затраченных часов не может быть позже текущего дня")
    private LocalDate date;
    @Min(value = 1, message = "Количество затраченных часов не должно быть меньше 1")
    @Max(value = 24, message = "Количество затраченных часов не должно быть больше 24")
    private Short spentTime;
    private Long employeeId;
    private Long taskId;
}
