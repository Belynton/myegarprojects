package ru.egar.domain.card.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import ru.egar.domain.card.dto.SaveTaskDailyCardDto;
import ru.egar.domain.card.dto.TaskDailyCardResponseDto;
import ru.egar.domain.card.entity.TaskDailyCard;
import ru.egar.domain.employee.mapper.EmployeeMapper;
import ru.egar.domain.task.mapper.TaskMapper;


@Mapper(
        componentModel = "spring",
        uses = {
                EmployeeMapper.class,
                TaskMapper.class
        },
        injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface TaskDailyCardMapper {

    TaskDailyCardResponseDto toTaskDailyCardResponseDto(TaskDailyCard taskDailyCard);

    TaskDailyCard toTaskDailyCard(SaveTaskDailyCardDto saveTaskDailyCardDto);

}
