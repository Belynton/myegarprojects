package ru.egar.domain.card.controller.mvc;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.egar.domain.card.dto.SaveTaskDailyCardDto;
import ru.egar.domain.card.service.TaskDailyCardService;


@Controller
@RequiredArgsConstructor
public class TaskDailyCardController {

    private final TaskDailyCardService taskDailyCardService;

    @PostMapping("/card")
    public String createCard(@Valid @ModelAttribute SaveTaskDailyCardDto saveCardDto) {
        var cardId = taskDailyCardService.create(saveCardDto);
        var taskId = taskDailyCardService.findById(cardId).getTask().getId();
        return "redirect:/task/" + taskId;
    }
}
