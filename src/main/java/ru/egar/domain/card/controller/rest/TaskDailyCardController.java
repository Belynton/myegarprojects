package ru.egar.domain.card.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.egar.domain.card.dto.SaveTaskDailyCardDto;
import ru.egar.domain.card.dto.TaskDailyCardResponseDto;

import java.util.List;

@RequestMapping("api/v1/task_daily_card")
public interface TaskDailyCardController {

    @Operation(
            summary = "Получить все записи о затраченном времени на выполнение задач",
            description = "Возвращает все записи о затраченном времени на выполнение задач",
            method = "GET"
    )
    @GetMapping
    ResponseEntity<List<TaskDailyCardResponseDto>> getTaskDailyCards();

    @Operation(
            summary = "Получить сумарное затраченное время на задачу",
            description = "Возвращает сумарное затраченное время на задачу по ее идентификатору",
            method = "GET"
    )
    @GetMapping("/sum")
    ResponseEntity<Long> getSummary(
            @RequestParam @Parameter(description = "Идентификатор задачи") Long taskId
    );

    @Operation(
            summary = "Получить сумарное время затраченное работником",
            description = "Возвращает сумарное время затраченное работником в течении месяца",
            method = "GET"
    )
    @GetMapping("/sum-by-employee")
    ResponseEntity<Long> getSummaryForEmployee(
            @RequestParam @Parameter(description = "Идентификатор работника") Long employeeId,
            @RequestParam @Parameter(description = "Номер месяца") Long monthNumber
    );

    @Operation(
            summary = "Получить записи о затраченном времени на выполнение задачи",
            description = "Возвращает запись о затраченном на задачу времени за указанный месяц",
            method = "GET"
    )
    @GetMapping("/by-task/{taskId}")
    ResponseEntity<List<TaskDailyCardResponseDto>> getAllTaskDailyCardByTaskIdAndMonthNumber(
            @PathVariable @Parameter(description = "Идентификатор задачи") Long taskId,
            @RequestParam @Parameter(description = "Номер месяца") Long monthNumber
    );

    @Operation(
            summary = "Получить записи о затраченном сотрудником времени на выполнение задач",
            description = "Возвращает запись о затраченном на задачи времени за указанный месяц для указанного сотрудника сотрудника",
            method = "GET"
    )
    @GetMapping("/by-employee/{employeeId}")
    ResponseEntity<List<TaskDailyCardResponseDto>> getAllTaskDailyCardByEmployeeIdAndMonthNumber(
            @PathVariable @Parameter(description = "Идентификатор сотрудника") Long employeeId,
            @RequestParam @Parameter(description = "Номер месяца") Long monthNumber
    );

    @Operation(
            summary = "Добавить запись о затраченном на задачу времени",
            description = "Добавляет запись о зтраченном на задачу времени",
            method = "POST"
    )
    @PostMapping
    ResponseEntity<Long> createTaskDailyCard(
            @Valid @RequestBody @Parameter(description = "Запись, которая будет добавлена") SaveTaskDailyCardDto saveCardDto
    );

    @Operation(
            summary = "Изменить запись о затраченном на задачу времени",
            description = "Изменить данные записи о затраченном на задачу времени по ее id",
            method = "POST"
    )
    @PatchMapping("/{id}")
    ResponseEntity<Long> updateTaskDailyCard(
            @PathVariable @Parameter(description = "Идентификатор записи") Long id,
            @Valid @RequestBody @Parameter(description = "Данные, которые необходимо заменить") SaveTaskDailyCardDto saveCardDto
    );

    @Operation(
            summary = "Удалить запись о затраченном на задачу времени",
            description = "Удалить запись о затраченном на задачу времени по его id",
            method = "DELETE"
    )
    @DeleteMapping("/{id}")
    ResponseEntity<Long> deleteTaskDailyCard(
            @PathVariable @Parameter(description = "Идентификатор записи") Long id
    );
}
