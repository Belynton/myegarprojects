package ru.egar.domain.card.controller.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.egar.domain.card.dto.SaveTaskDailyCardDto;
import ru.egar.domain.card.dto.TaskDailyCardResponseDto;
import ru.egar.domain.card.service.TaskDailyCardService;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class TaskDailyCardControllerImpl implements TaskDailyCardController {

    private final TaskDailyCardService service;

    @Override
    public ResponseEntity<List<TaskDailyCardResponseDto>> getTaskDailyCards() {
        return new ResponseEntity<>(service.getAll(), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> getSummary(Long taskId) {
        return new ResponseEntity<>(service.getSummary(taskId), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> getSummaryForEmployee(Long employeeId, Long monthNumber) {
        return new ResponseEntity<>(service.getSummaryForEmployee(employeeId, monthNumber), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<List<TaskDailyCardResponseDto>> getAllTaskDailyCardByTaskIdAndMonthNumber(Long taskId, Long monthNumber) {
        return new ResponseEntity<>(service.getAllByTaskIdAndMonthNumber(taskId, monthNumber), HttpStatusCode.valueOf(200));

    }

    @Override
    public ResponseEntity<List<TaskDailyCardResponseDto>> getAllTaskDailyCardByEmployeeIdAndMonthNumber(Long employeeId, Long monthNumber) {
        return new ResponseEntity<>(service.getAllByEmployeeIdAndMonthNumber(employeeId, monthNumber), HttpStatusCode.valueOf(200));

    }

    @Override
    public ResponseEntity<Long> createTaskDailyCard(SaveTaskDailyCardDto saveCardDto) {
        return new ResponseEntity<>(service.create(saveCardDto), HttpStatusCode.valueOf(202));
    }

    @Override
    public ResponseEntity<Long> updateTaskDailyCard(Long id, SaveTaskDailyCardDto saveCardDto) {
        return new ResponseEntity<>(service.update(id, saveCardDto), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> deleteTaskDailyCard(Long id) {
        service.delete(id);
        return new ResponseEntity<>(id, HttpStatusCode.valueOf(200));
    }
}
