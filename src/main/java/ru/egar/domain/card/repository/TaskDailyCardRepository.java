package ru.egar.domain.card.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.egar.domain.card.entity.TaskDailyCard;

import java.util.List;

@Repository
public interface TaskDailyCardRepository extends JpaRepository<TaskDailyCard, Long> {

    @Query(
            """
            SELECT tdc FROM TaskDailyCard tdc
            left join fetch tdc.task t
            left join fetch tdc.employee e
            where MONTH(tdc.date) = :monthNumber
            and t.id = :taskId
            """
    )
    List<TaskDailyCard> findAllByTaskIdAndMonthNumber(@Param("taskId") Long taskId,
                                                      @Param("monthNumber") Long monthNumber);

    @Query(
            """
            SELECT tdc FROM TaskDailyCard tdc
            left join fetch tdc.task t
            left join fetch tdc.employee e
            where MONTH(tdc.date) = :monthNumber
            and e.id = :employeeId
            """
    )
    List<TaskDailyCard> findAllByEmployeeIdAndMonthNumber(@Param("employeeId") Long employeeId,
                                                          @Param("monthNumber") Long monthNumber);

    @Query(
            """
            SELECT sum(tdc.spentTime) from TaskDailyCard tdc
            WHERE tdc.task.id = :taskId
            """
    )
    Long getSummaryTaskDuration(@Param("taskId") Long taskId);

    @Query(
            """
            SELECT sum(tdc.spentTime) from TaskDailyCard tdc
            WHERE MONTH(tdc.date) = :monthNumber
            AND tdc.employee.id = :employeeId
            """
    )
    Long getSummaryWorkDurationForEmployeeByMonth(@Param("employeeId") Long employeeId,
                                                  @Param("monthNumber") Long monthNumber);
}
