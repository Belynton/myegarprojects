package ru.egar.domain.overwork.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.egar.domain.overwork.entity.Overwork;

public interface OverworkRepository extends JpaRepository<Overwork, Long> {
}
