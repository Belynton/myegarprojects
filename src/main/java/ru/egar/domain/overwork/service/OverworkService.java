package ru.egar.domain.overwork.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.egar.domain.employee.entity.Employee;
import ru.egar.domain.employee.mapper.EmployeeMapper;
import ru.egar.domain.employee.service.EmployeeService;
import ru.egar.domain.overwork.dto.OverworkFullResponseDto;
import ru.egar.domain.overwork.dto.OverworkShortResponseDto;
import ru.egar.domain.overwork.dto.SaveOverworkDto;
import ru.egar.domain.overwork.entity.Overwork;
import ru.egar.domain.overwork.mapper.OverworkMapper;
import ru.egar.domain.overwork.repository.OverworkRepository;
import ru.egar.exception.types.BadTimeInputException;
import ru.egar.exception.types.ElementNotFoundException;
import ru.egar.utils.NullAwareBeanUtils;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OverworkService {

    private final String NOT_FOUND_MESSAGE = "Переработка с id = '%s' не найдена";

    private final OverworkRepository overworkRepository;
    private final OverworkMapper overworkMapper;
    private final EmployeeService employeeService;
    private final EmployeeMapper employeeMapper;

    public OverworkFullResponseDto getById(Long id) {
        var overwork = findById(id);
        var employeeShort = employeeMapper.toShortEmployee(overwork.getEmployee());
        var fullOverwork = overworkMapper.toFullOverwork(overwork);
        fullOverwork.setEmployee(employeeShort);
        return fullOverwork;
    }

    public List<OverworkShortResponseDto> getAll() {
        return overworkRepository.findAll()
                .stream()
                .map(overworkMapper::toShortOverwork)
                .toList();
    }

    public Long create(SaveOverworkDto saveOverworkDto) {
        var employee = employeeService.findById(saveOverworkDto.getEmployeeId());
        throwIfOverworkCantBeCreated(employee, saveOverworkDto.getDate());
        var overworkToSave = overworkMapper.toOverwork(saveOverworkDto);
        overworkToSave.setEmployee(employee);
        var savedOverwork = overworkRepository.save(overworkToSave);
        return savedOverwork.getId();
    }

    public Long update(Long id, SaveOverworkDto saveOverworkDto) {
        var overwork = findById(id);
        NullAwareBeanUtils.copyNonNullProperties(overworkMapper.toOverwork(saveOverworkDto), overwork);
        Optional.ofNullable(saveOverworkDto.getEmployeeId())
                .ifPresent(dtoId -> overwork.setEmployee(employeeService.findById(dtoId)));
        var savedEmployee = overworkRepository.save(overwork);
        return savedEmployee.getId();
    }

    public void delete(Long id) {
        var overwork = findById(id);
        overworkRepository.delete(overwork);
    }

    public Overwork findById(Long id) {
        return overworkRepository.findById(id)
                .orElseThrow(() -> new ElementNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));
    }

    private void throwIfOverworkCantBeCreated(Employee employee, LocalDate saveOverworkDate) {
        if (employee.isOffdayAlreadyCreated(saveOverworkDate)) {
            throw new BadTimeInputException("Невозможно указать переработку в пропущенный день");
        }
        if (employee.isOverworkAlreadyCreated(saveOverworkDate)) {
            throw new BadTimeInputException("Переработка на указанную дату уже существует");
        }
    }
}
