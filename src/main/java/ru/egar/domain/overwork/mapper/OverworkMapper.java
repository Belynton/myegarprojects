package ru.egar.domain.overwork.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.egar.domain.overwork.dto.OverworkFullResponseDto;
import ru.egar.domain.overwork.dto.OverworkShortResponseDto;
import ru.egar.domain.overwork.dto.SaveOverworkDto;
import ru.egar.domain.overwork.entity.Overwork;

@Mapper(componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface OverworkMapper {
    @Mapping(target = "employee", ignore = true)
    OverworkFullResponseDto toFullOverwork(Overwork overwork);

    OverworkShortResponseDto toShortOverwork(Overwork overwork);

    Overwork toOverwork(SaveOverworkDto saveOverworkDto);
}
