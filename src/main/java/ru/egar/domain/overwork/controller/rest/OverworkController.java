package ru.egar.domain.overwork.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.egar.domain.overwork.dto.OverworkFullResponseDto;
import ru.egar.domain.overwork.dto.OverworkShortResponseDto;
import ru.egar.domain.overwork.dto.SaveOverworkDto;

import java.util.List;

@RequestMapping("api/v1/overwork")
public interface OverworkController {

    @Operation(
            summary = "Получить список всех переработок",
            description = "Возвращает список всех переработок",
            method = "GET"
    )
    @GetMapping
    ResponseEntity<List<OverworkShortResponseDto>> getOverworks();

    @Operation(
            summary = "Получить переработку по идентификатору",
            description = "Возвращает переработку с указанным идентификатором",
            method = "GET"
    )
    @GetMapping("/{id}")
    ResponseEntity<OverworkFullResponseDto> getOverworkById(
            @PathVariable @Parameter(description = "Идентификатор переработки дня") Long id
    );

    @Operation(
            summary = "Добавить переработку",
            description = "Добавляет переработку для сотрудника",
            method = "POST"
    )
    @PostMapping
    ResponseEntity<Long> createOverwork(
            @Valid @RequestBody @Parameter(description = "переработка, которая будет добавлена") SaveOverworkDto overworkDto
    );

    @Operation(
            summary = "Изменить переработку",
            description = "Изменить данные о переработке по ее id",
            method = "POST"
    )
    @PatchMapping("/{id}")
    ResponseEntity<Long> updateOverwork(
            @PathVariable @Parameter(description = "Идентификатор переработки") Long id,
            @Valid @RequestBody @Parameter(description = "Данные, которые необходимо заменить") SaveOverworkDto overworkDto
    );

    @Operation(
            summary = "Удалить переработку",
            description = "Удалить переработку по ее id",
            method = "DELETE"
    )
    @DeleteMapping("/{id}")
    ResponseEntity<Long> deleteOverwork(
            @PathVariable @Parameter(description = "Идентификатор переработки") Long id
    );

}
