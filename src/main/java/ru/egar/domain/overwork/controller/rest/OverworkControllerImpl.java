package ru.egar.domain.overwork.controller.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.egar.domain.overwork.dto.OverworkFullResponseDto;
import ru.egar.domain.overwork.dto.OverworkShortResponseDto;
import ru.egar.domain.overwork.dto.SaveOverworkDto;
import ru.egar.domain.overwork.service.OverworkService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class OverworkControllerImpl implements OverworkController {

    private final OverworkService overworkService;

    @Override
    public ResponseEntity<List<OverworkShortResponseDto>> getOverworks() {
        return new ResponseEntity<>(overworkService.getAll(), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<OverworkFullResponseDto> getOverworkById(Long id) {
        return new ResponseEntity<>(overworkService.getById(id), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> createOverwork(SaveOverworkDto overworkDto) {
        return new ResponseEntity<>(overworkService.create(overworkDto), HttpStatusCode.valueOf(202));
    }

    @Override
    public ResponseEntity<Long> updateOverwork(Long id, SaveOverworkDto overworkDto) {
        return new ResponseEntity<>(overworkService.update(id, overworkDto), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> deleteOverwork(Long id) {
        overworkService.delete(id);
        return new ResponseEntity<>(id, HttpStatusCode.valueOf(200));
    }
}
