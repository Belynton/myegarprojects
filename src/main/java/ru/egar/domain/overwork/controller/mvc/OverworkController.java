package ru.egar.domain.overwork.controller.mvc;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.egar.domain.overwork.dto.SaveOverworkDto;
import ru.egar.domain.overwork.service.OverworkService;

@Controller
@RequiredArgsConstructor
public class OverworkController {

    private final OverworkService overworkService;

    @PostMapping("/overwork")
    public String createOverwork(@Valid @ModelAttribute SaveOverworkDto saveOverworkDto) {
        var overworkId = overworkService.create(saveOverworkDto);
        var employeeId = overworkService.findById(overworkId).getEmployee().getId();
        return "redirect:/employee/" + employeeId;
    }
}
