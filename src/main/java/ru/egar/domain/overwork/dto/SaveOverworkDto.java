package ru.egar.domain.overwork.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.PastOrPresent;
import lombok.Data;

import java.time.LocalDate;

@Data
public class SaveOverworkDto {
    @PastOrPresent(message = "Дата переработки не может быть позже текущего дня")
    private LocalDate date;
    @Min(value = 1, message = "Число должно быть больше или равно 1")
    @Max(value = 1440, message = "Число должно быть меньше или равно 1440")
    private Short duration;
    private Long employeeId;
}
