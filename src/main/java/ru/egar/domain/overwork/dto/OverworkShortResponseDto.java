package ru.egar.domain.overwork.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class OverworkShortResponseDto {

    private Long id;
    private LocalDate date;
    private Short duration;
}
