package ru.egar.domain.overwork.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.egar.domain.employee.entity.Employee;

import java.time.LocalDate;

/**
 * Сущность переработок сотрудника.
 * Включает в себя: Дату переработки и время переработки(в минутах).
 * Связана с сотрудником, для которого эта переработка применяется.
 */
@Getter
@Setter
@RequiredArgsConstructor
@Entity
public class Overwork {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private LocalDate date;
    @Column(nullable = false)
    private Short duration;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;
}
