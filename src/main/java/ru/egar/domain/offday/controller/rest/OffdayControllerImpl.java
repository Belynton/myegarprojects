package ru.egar.domain.offday.controller.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.egar.domain.offday.dto.OffdayFullResponseDto;
import ru.egar.domain.offday.dto.OffdayShortResponseDto;
import ru.egar.domain.offday.dto.SaveOffdayDto;
import ru.egar.domain.offday.service.OffdayService;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class OffdayControllerImpl implements OffdayController {

    private final OffdayService offdayService;

    @Override
    public ResponseEntity<List<OffdayShortResponseDto>> getOffdays() {
        return new ResponseEntity<>(offdayService.getAll(), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<OffdayFullResponseDto> getOffdayById(Long id) {
        return new ResponseEntity<>(offdayService.getById(id), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> createOffday(SaveOffdayDto offdayDto) {
        return new ResponseEntity<>(offdayService.create(offdayDto), HttpStatusCode.valueOf(202));
    }

    @Override
    public ResponseEntity<Long> updateOffday(Long id, SaveOffdayDto offdayDto) {
        return new ResponseEntity<>(offdayService.update(id, offdayDto), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> deleteOffday(Long id) {
        offdayService.delete(id);
        return new ResponseEntity<>(id, HttpStatusCode.valueOf(200));
    }
}
