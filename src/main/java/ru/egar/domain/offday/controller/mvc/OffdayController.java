package ru.egar.domain.offday.controller.mvc;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.egar.domain.offday.dto.SaveOffdayDto;
import ru.egar.domain.offday.service.OffdayService;

@Controller
@RequiredArgsConstructor
public class OffdayController {

    private final OffdayService offdayService;

    @PostMapping("/offday")
    public String createOffday(@Valid @ModelAttribute SaveOffdayDto saveOffdayDto) {
        var offdayId = offdayService.create(saveOffdayDto);
        var employeeId = offdayService.findById(offdayId).getEmployee().getId();
        return "redirect:/employee/" + employeeId;
    }
}
