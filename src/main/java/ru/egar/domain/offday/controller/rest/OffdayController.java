package ru.egar.domain.offday.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.egar.domain.offday.dto.OffdayFullResponseDto;
import ru.egar.domain.offday.dto.OffdayShortResponseDto;
import ru.egar.domain.offday.dto.SaveOffdayDto;

import java.util.List;

@RequestMapping("api/v1/offday")
public interface OffdayController {

    @Operation(
            summary = "Получить список всех пропущенных дней",
            description = "Возвращает список всех пропущенных дней",
            method = "GET"
    )
    @GetMapping
    ResponseEntity<List<OffdayShortResponseDto>> getOffdays();

    @Operation(
            summary = "Получить пропущенный день по идентификатору",
            description = "Возвращает пропущенный день с указанным идентификатором",
            method = "GET"
    )
    @GetMapping("/{id}")
    ResponseEntity<OffdayFullResponseDto> getOffdayById(
            @PathVariable @Parameter(description = "Идентификатор пропущенного дня") Long id
    );

    @Operation(
            summary = "Добавить пропущенный день",
            description = "Добавляет пропущенный день для сотрудника",
            method = "POST"
    )
    @PostMapping
    ResponseEntity<Long> createOffday(
            @Valid @RequestBody @Parameter(description = "Пропущенный день, который будет добавлен") SaveOffdayDto offdayDto
    );

    @Operation(
            summary = "Изменить пропущенный день",
            description = "Изменить данные о пропущенном дне по его id",
            method = "POST"
    )
    @PatchMapping("/{id}")
    ResponseEntity<Long> updateOffday(
            @PathVariable @Parameter(description = "Идентификатор пропущенного дня") Long id,
            @Valid @RequestBody @Parameter(description = "Данные, которые необходимо заменить") SaveOffdayDto offdayDto
    );

    @Operation(
            summary = "Удалить пропущенный день",
            description = "Удалить пропушенный день по его id",
            method = "DELETE"
    )
    @DeleteMapping("/{id}")
    ResponseEntity<Long> deleteOffday(
            @PathVariable @Parameter(description = "Идентификатор пропущенного дня") Long id
    );
}
