package ru.egar.domain.offday.enums;

import lombok.Getter;

@Getter
public enum OffdayType {

    SKIP("Отгул"), HOSPITALS("Больничный"), VACATION("Отпуск");

    private final String name;

    OffdayType(String name) {
        this.name = name;
    }

}
