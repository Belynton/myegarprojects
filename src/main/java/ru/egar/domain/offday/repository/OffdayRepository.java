package ru.egar.domain.offday.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.egar.domain.offday.entity.Offday;

@Repository
public interface OffdayRepository extends JpaRepository<Offday, Long> {
}
