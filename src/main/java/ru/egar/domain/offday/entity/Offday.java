package ru.egar.domain.offday.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.egar.domain.employee.entity.Employee;
import ru.egar.domain.offday.enums.OffdayType;

import java.time.LocalDate;

/**
 * Сущность пропусков сотрудника.
 * Включает в себя: дату пропуска, оплачиваемость, причину.
 * Связана с сотрудником, для которого этот пропуск применяется.
 */
@Getter
@Setter
@Entity
@RequiredArgsConstructor
public class Offday {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private LocalDate date;
    @Column(nullable = false)
    private OffdayType type;
    @Column(nullable = false)
    private Boolean isPayable;
    private String reason;
    @ManyToOne
    @JoinColumn(name = "employee_id", nullable = false)
    private Employee employee;

    public void addEmployee(Employee employee) {
        this.employee = employee;
        employee.getOffdays().add(this);
    }

    public void removeEmployee(Employee employee) {
        this.employee = null;
        employee.getOffdays().remove(this);
    }

}
