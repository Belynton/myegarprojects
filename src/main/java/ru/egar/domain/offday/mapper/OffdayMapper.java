package ru.egar.domain.offday.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.egar.domain.employee.mapper.EmployeeMapper;
import ru.egar.domain.offday.dto.OffdayFullResponseDto;
import ru.egar.domain.offday.dto.OffdayShortResponseDto;
import ru.egar.domain.offday.dto.SaveOffdayDto;
import ru.egar.domain.offday.entity.Offday;

@Mapper(componentModel = "spring",
        uses = EmployeeMapper.class,
        injectionStrategy = InjectionStrategy.CONSTRUCTOR

)
public interface OffdayMapper {
    @Mapping(target = "employee", ignore = true)
    OffdayFullResponseDto toFullOffday(Offday offday);

    OffdayShortResponseDto toShortOffday(Offday offday);

    Offday toOffday(SaveOffdayDto saveOffdayDto);
}
