package ru.egar.domain.offday.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.egar.domain.employee.mapper.EmployeeMapper;
import ru.egar.domain.employee.service.EmployeeService;
import ru.egar.domain.offday.dto.OffdayFullResponseDto;
import ru.egar.domain.offday.dto.OffdayShortResponseDto;
import ru.egar.domain.offday.dto.SaveOffdayDto;
import ru.egar.domain.offday.entity.Offday;
import ru.egar.domain.offday.mapper.OffdayMapper;
import ru.egar.domain.offday.repository.OffdayRepository;
import ru.egar.exception.types.BadTimeInputException;
import ru.egar.exception.types.ElementNotFoundException;
import ru.egar.utils.NullAwareBeanUtils;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OffdayService {

    private final String NOT_FOUND_MESSAGE = "Пропущенный день с id = '%s' не найден";

    private final OffdayRepository offdayRepository;
    private final OffdayMapper offdayMapper;
    private final EmployeeService employeeService;
    private final EmployeeMapper employeeMapper;

    public OffdayFullResponseDto getById(Long id) {
        var offday = findById(id);
        var employeeShort = employeeMapper.toShortEmployee(offday.getEmployee());
        var fullOffday = offdayMapper.toFullOffday(offday);
        fullOffday.setEmployee(employeeShort);
        return fullOffday;
    }

    public List<OffdayShortResponseDto> getAll() {
        return offdayRepository.findAll()
                .stream()
                .map(offdayMapper::toShortOffday)
                .toList();
    }

    @Transactional
    public Long create(SaveOffdayDto saveOffdayDto) {
        var employee = employeeService.findById(saveOffdayDto.getEmployeeId());
        if (employee.isOffdayAlreadyCreated(saveOffdayDto.getDate())) {
            throw new BadTimeInputException("Пропуск на указанную дату уже существует");
        }
        var offdayToSave = offdayMapper.toOffday(saveOffdayDto);
        offdayToSave.setEmployee(employee);
        var savedOffday = offdayRepository.save(offdayToSave);
        return savedOffday.getId();
    }

    @Transactional
    public Long update(Long id, SaveOffdayDto saveOffdayDto) {
        var offday = findById(id);
        NullAwareBeanUtils.copyNonNullProperties(offdayMapper.toOffday(saveOffdayDto), offday);
        Optional.ofNullable(saveOffdayDto.getEmployeeId())
                .ifPresent(dtoId -> offday.setEmployee(employeeService.findById(dtoId)));
        var savedOffday = offdayRepository.save(offday);
        return savedOffday.getId();
    }

    @Transactional
    public void delete(Long id) {
        var offday = findById(id);
        offdayRepository.delete(offday);
    }

    public Offday findById(Long id) {
        return offdayRepository.findById(id)
                .orElseThrow(() -> new ElementNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));
    }

}
