package ru.egar.domain.offday.dto;

import lombok.Data;
import ru.egar.domain.offday.enums.OffdayType;

import java.time.LocalDate;

@Data
public class OffdayShortResponseDto {
    private Long id;
    private LocalDate date;
    private OffdayType type;
    private String reason;
}
