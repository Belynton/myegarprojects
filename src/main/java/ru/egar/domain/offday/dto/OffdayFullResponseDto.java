package ru.egar.domain.offday.dto;

import lombok.Data;
import ru.egar.domain.employee.dto.EmployeeShortResponseDto;
import ru.egar.domain.offday.enums.OffdayType;

import java.time.LocalDate;

@Data
public class OffdayFullResponseDto {
    private Long id;
    private LocalDate date;
    private OffdayType type;
    private Boolean isPayable;
    private String reason;
    private EmployeeShortResponseDto employee;
}
