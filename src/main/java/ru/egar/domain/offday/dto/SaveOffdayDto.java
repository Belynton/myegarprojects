package ru.egar.domain.offday.dto;

import lombok.Data;
import ru.egar.domain.offday.enums.OffdayType;

import java.time.LocalDate;

@Data
public class SaveOffdayDto {
    private LocalDate date;
    private OffdayType type;
    private Boolean isPayable;
    private String reason;
    private Long employeeId;
}
