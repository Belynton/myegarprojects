package ru.egar.domain.task.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import ru.egar.domain.employee.entity.Employee;
import ru.egar.domain.task.enums.TaskDifficulty;
import ru.egar.domain.task.enums.TaskImportance;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Сущность задачи сотрудника.
 * Включает в себя: важность задачи, сложность, описание, время начала, время окончания.
 * Связана с сотрудником, к которому эта задача привязана.
 * Связь M:M подразумевает возможность выполнения одной задачи несколькими сотрудниками.
 */
@Getter
@Setter
@Entity
@RequiredArgsConstructor
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private TaskImportance importance;
    @Column(nullable = false)
    private TaskDifficulty difficulty;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    private LocalDateTime startTime;
    private LocalDateTime finishTime;
    @ManyToMany(mappedBy = "tasks", fetch = FetchType.LAZY)
    private Set<Employee> employees = new HashSet<>();

    public void addEmployee(Employee employee) {
        this.employees.add(employee);
        employee.getTasks().add(this);
    }

    public void removeEmployee(Employee employee) {
        this.employees.remove(employee);
        employee.getTasks().remove(this);
    }

    public void clearEmployees() {
        while (employees.iterator().hasNext()) {
            employees.iterator().next().removeTask(this);
        }
    }
}
