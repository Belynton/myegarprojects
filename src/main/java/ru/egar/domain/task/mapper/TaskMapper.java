package ru.egar.domain.task.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.egar.domain.task.dto.SaveTaskDto;
import ru.egar.domain.task.dto.TaskFullResponseDto;
import ru.egar.domain.task.dto.TaskShortResponseDto;
import ru.egar.domain.task.entity.Task;

@Mapper(componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface TaskMapper {

    @Mapping(target = "employees", ignore = true)
    TaskFullResponseDto toFullTask(Task task);

    TaskShortResponseDto toShortTask(Task task);

    @Mapping(target = "employees", ignore = true)
    Task toTask(SaveTaskDto saveTaskDto);

}
