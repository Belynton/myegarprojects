package ru.egar.domain.task.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.egar.domain.employee.mapper.EmployeeMapper;
import ru.egar.domain.employee.service.EmployeeService;
import ru.egar.domain.task.dto.SaveTaskDto;
import ru.egar.domain.task.dto.TaskFullResponseDto;
import ru.egar.domain.task.dto.TaskShortResponseDto;
import ru.egar.domain.task.entity.Task;
import ru.egar.domain.task.mapper.TaskMapper;
import ru.egar.domain.task.repository.TaskRepository;
import ru.egar.exception.types.AlreadyFinishedTaskException;
import ru.egar.exception.types.BadTimeInputException;
import ru.egar.exception.types.ElementNotFoundException;
import ru.egar.utils.NullAwareBeanUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final String NOT_FOUND_MESSAGE = "Задача с id = '%s' не найдена";
    private final String FINISH_TASK_IN_FUTURE_MESSAGE = "Указанное время завершения задачи не может быть позже текущего";

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final EmployeeService employeeService;
    private final EmployeeMapper employeeMapper;

    public TaskFullResponseDto getById(Long id) {
        var task = findById(id);
        var employeesShort = task.getEmployees().stream()
                .map(employeeMapper::toShortEmployee)
                .collect(Collectors.toSet());
        var taskFull = taskMapper.toFullTask(task);
        taskFull.setEmployees(employeesShort);
        return taskFull;
    }

    public List<TaskShortResponseDto> getAll() {
        return taskRepository.findAll()
                .stream()
                .map(taskMapper::toShortTask)
                .toList();
    }

    public Long create(SaveTaskDto saveTaskDto) {
        var employees = saveTaskDto.getEmployees()
                .stream()
                .map(employeeService::findById)
                .collect(Collectors.toSet());
        var taskToSave = taskMapper.toTask(saveTaskDto);
        taskToSave.setEmployees(employees);
        var savedTask = taskRepository.save(taskToSave);
        return savedTask.getId();
    }

    public Long update(Long id, SaveTaskDto saveTaskDto) {
        var task = findById(id);
        task.clearEmployees();
        Optional.ofNullable(saveTaskDto.getEmployees())
                .ifPresent(longs -> longs
                        .forEach(empId -> task.addEmployee(employeeService.findById(empId))));
        NullAwareBeanUtils.copyNonNullProperties(taskMapper.toTask(saveTaskDto), task);
        var savedTask = taskRepository.save(task);
        return savedTask.getId();
    }

    public Long addEmployeeToTask(Long employeeId, Long taskId) {
        var task = findById(taskId);
        var employee = employeeService.findById(employeeId);
        task.addEmployee(employee);
        var savedTask = taskRepository.save(task);
        return savedTask.getId();
    }

    public Long finishTask(Long id, LocalDateTime finishTime) {
        var task = findById(id);
        if (task.getFinishTime() != null) {
            throw new AlreadyFinishedTaskException(String.format("Задача с id = %s уже завершена", id));
        }
        task.setFinishTime(getValidFinishTimeOrThrow(finishTime));
        var savedTask = taskRepository.save(task);
        return savedTask.getId();
    }

    public void delete(Long id) {
        var task = findById(id);
        taskRepository.delete(task);
    }

    public Task findById(Long id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new ElementNotFoundException(String.format(NOT_FOUND_MESSAGE, id)));
    }

    private LocalDateTime getValidFinishTimeOrThrow(LocalDateTime finishTime) {
        var currentTime = LocalDateTime.now();
        if (finishTime == null) {
            return currentTime;
        }
        if (finishTime.isAfter(currentTime)) {
            throw new BadTimeInputException(FINISH_TASK_IN_FUTURE_MESSAGE);
        }
        return finishTime;
    }
}
