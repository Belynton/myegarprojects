package ru.egar.domain.task.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.PastOrPresent;
import lombok.Data;
import ru.egar.domain.task.enums.TaskDifficulty;
import ru.egar.domain.task.enums.TaskImportance;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class SaveTaskDto {
    private TaskImportance importance;
    private TaskDifficulty difficulty;
    @NotEmpty
    private String description;
    @PastOrPresent(message = "Дата начала выполнения задачи не может быть позже текущего дня")
    private LocalDateTime startTime;
    private List<Long> employees;
}
