package ru.egar.domain.task.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import ru.egar.domain.employee.dto.EmployeeShortResponseDto;
import ru.egar.domain.task.enums.TaskDifficulty;
import ru.egar.domain.task.enums.TaskImportance;

import java.time.LocalDateTime;
import java.util.Set;

@Data
public class TaskFullResponseDto {
    private Long id;
    private TaskImportance importance;
    private TaskDifficulty difficulty;
    private String description;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime finishTime;
    private Set<EmployeeShortResponseDto> employees;
}
