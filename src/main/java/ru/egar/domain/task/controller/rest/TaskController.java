package ru.egar.domain.task.controller.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.egar.domain.task.dto.SaveTaskDto;
import ru.egar.domain.task.dto.TaskFullResponseDto;
import ru.egar.domain.task.dto.TaskShortResponseDto;

import java.time.LocalDateTime;
import java.util.List;

@RequestMapping("api/v1/task")
public interface TaskController {

    @Operation(
            summary = "Получить список всех задач",
            description = "Возвращает список всех задач",
            method = "GET"
    )
    @GetMapping
    ResponseEntity<List<TaskShortResponseDto>> getTasks();

    @Operation(
            summary = "Получить задачу по идентификатору",
            description = "Возвращает задачу с указанным идентификатором",
            method = "GET"
    )
    @GetMapping("/{id}")
    ResponseEntity<TaskFullResponseDto> getTaskById(
            @PathVariable @Parameter(description = "Идентификатор задачи") Long id
    );

    @Operation(
            summary = "Добавить новую задачу",
            description = "Добавляет новую задачу",
            method = "POST"
    )
    @PostMapping
    ResponseEntity<Long> createTask(
            @Valid @RequestBody @Parameter(description = "Задача, которая будет добавлена") SaveTaskDto task
    );

    @Operation(
            summary = "Изменить задачу",
            description = "Изменить данные задачи по ее id",
            method = "POST"
    )
    @PatchMapping("/{id}")
    ResponseEntity<Long> updateTask(
            @PathVariable @Parameter(description = "Идентификатор задачи") Long id,
            @Valid @RequestBody @Parameter(description = "Данные, которые необходимо заменить") SaveTaskDto task
    );

    @Operation(
            summary = "Добавить сотрудника",
            description = "Добавить сотрудника к задаче по id",
            method = "POST"
    )
    @PostMapping("/add-employee")
    ResponseEntity<Long> addEmployeeToTask(
            @RequestParam @Parameter(description = "Идентификатор сотрудника") Long employeeId,
            @RequestParam @Parameter(description = "Идентификатор задачи") Long taskId
    );

    @Operation(
            summary = "Закрыть задачу",
            description = "Закрыть задачу по ее id с указанным временем",
            method = "POST"
    )
    @PatchMapping("/finish/{id}")
    ResponseEntity<Long> finishTask(
            @PathVariable @Parameter(description = "Идентификатор задачи") Long id,
            @RequestBody(required = false)
            @Parameter(description = "Время завершения задачи. Если не указано, то будет указано текущее время") LocalDateTime finishTime
    );

    @Operation(
            summary = "Удалить задачу",
            description = "Удалить задачу по ее id",
            method = "DELETE"
    )
    @DeleteMapping("/{id}")
    ResponseEntity<Long> deleteTask(
            @PathVariable @Parameter(description = "Идентификатор задачи") Long id
    );

}
