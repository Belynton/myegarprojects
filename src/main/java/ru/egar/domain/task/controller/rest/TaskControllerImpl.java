package ru.egar.domain.task.controller.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import ru.egar.domain.task.dto.SaveTaskDto;
import ru.egar.domain.task.dto.TaskFullResponseDto;
import ru.egar.domain.task.dto.TaskShortResponseDto;
import ru.egar.domain.task.service.TaskService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class TaskControllerImpl implements TaskController {

    private final TaskService taskService;

    @Override
    public ResponseEntity<List<TaskShortResponseDto>> getTasks() {
        return new ResponseEntity<>(taskService.getAll(), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<TaskFullResponseDto> getTaskById(Long id) {
        return new ResponseEntity<>(taskService.getById(id), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> createTask(SaveTaskDto task) {
        return new ResponseEntity<>(taskService.create(task), HttpStatusCode.valueOf(202));
    }

    @Override
    public ResponseEntity<Long> updateTask(Long id, SaveTaskDto task) {
        return new ResponseEntity<>(taskService.update(id, task), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> addEmployeeToTask(Long employeeId, Long taskId) {
        return new ResponseEntity<>(taskService.addEmployeeToTask(employeeId, taskId), HttpStatusCode.valueOf(200));
    }

    @Override
    public ResponseEntity<Long> finishTask(Long id, LocalDateTime finishTime) {
        return new ResponseEntity<>(taskService.finishTask(id, finishTime), HttpStatusCode.valueOf(202));
    }

    @Override
    public ResponseEntity<Long> deleteTask(Long id) {
        taskService.delete(id);
        return new ResponseEntity<>(id, HttpStatusCode.valueOf(200));
    }
}
