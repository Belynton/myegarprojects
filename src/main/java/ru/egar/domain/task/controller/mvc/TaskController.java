package ru.egar.domain.task.controller.mvc;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.egar.domain.card.dto.SaveTaskDailyCardDto;
import ru.egar.domain.card.service.TaskDailyCardService;
import ru.egar.domain.employee.dto.EmployeeShortResponseDto;
import ru.egar.domain.employee.service.EmployeeService;
import ru.egar.domain.task.dto.SaveTaskDto;
import ru.egar.domain.task.enums.TaskDifficulty;
import ru.egar.domain.task.enums.TaskImportance;
import ru.egar.domain.task.service.TaskService;

import java.time.LocalDateTime;

@Controller
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;
    private final TaskDailyCardService taskDailyCardService;
    private final EmployeeService employeeService;

    @GetMapping("/task")
    public String getAllTasks(Model model) {
        model.addAttribute("taskList", taskService.getAll());
        model.addAttribute("employees", employeeService.getAll());
        model.addAttribute("difficultyVariants", TaskDifficulty.values());
        model.addAttribute("importanceVariants", TaskImportance.values());
        model.addAttribute("saveTaskDto", new SaveTaskDto());
        model.addAttribute("employeeShort", new EmployeeShortResponseDto());
        return "task";
    }

    @PostMapping("/task")
    public String createTask(@Valid @ModelAttribute SaveTaskDto saveTaskDto) {
        taskService.create(saveTaskDto);
        return "redirect:/task";
    }

    @GetMapping("/task/{id}")
    public String getTaskById(@PathVariable Long id, Model model) {
        model.addAttribute("employees", employeeService.getAll());
        model.addAttribute("selectedTask", taskService.getById(id));
        model.addAttribute("summaryTime", taskDailyCardService.getSummary(id));
        model.addAttribute("saveCardDto", new SaveTaskDailyCardDto());
        return "selectedTask";
    }

    @PostMapping("/task/finish/{id}")
    public String finishTaskById(@PathVariable Long id) {
        taskService.finishTask(id, LocalDateTime.now());
        return "redirect:/task/" + id;
    }

    @PostMapping("/task/add-employee")
    public String addEmployeeToTask(
            @RequestParam(value = "employeeId") Long employeeId,
            @RequestParam(value = "taskId") Long taskId
    ) {
        taskService.addEmployeeToTask(employeeId, taskId);
        return "redirect:/task/" + taskId;
    }

}