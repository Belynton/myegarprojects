package ru.egar.domain.task.enums;

import lombok.Getter;

@Getter
public enum TaskImportance {
    RUSH("Очень важная"), NORMAL("Обычная");

    private final String name;

    TaskImportance(String name) {
        this.name = name;
    }
}
