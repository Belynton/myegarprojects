package ru.egar.domain.task.enums;

import lombok.Getter;

@Getter
public enum TaskDifficulty {
    EASY("Низкая"), MEDIUM("Средняя"), HARD("Высокая");

    private final String name;

    TaskDifficulty(String name) {
        this.name = name;
    }

}
