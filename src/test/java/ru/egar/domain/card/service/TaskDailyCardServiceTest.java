package ru.egar.domain.card.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.egar.domain.card.dto.SaveTaskDailyCardDto;
import ru.egar.domain.employee.entity.Employee;
import ru.egar.domain.employee.service.EmployeeService;
import ru.egar.domain.offday.entity.Offday;
import ru.egar.domain.task.entity.Task;
import ru.egar.domain.task.service.TaskService;
import ru.egar.exception.types.BadTimeInputException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class TaskDailyCardServiceTest {
    @Mock
    private EmployeeService employeeService;
    @Mock
    private TaskService taskService;
    @InjectMocks
    private TaskDailyCardService service;


    private Task task;
    private Employee employee;
    private Offday offday;
    private LocalDateTime startTime;

    @BeforeEach
    public void setup() {
        startTime = LocalDateTime.now();

        task = new Task();
        task.setId(1L);
        task.setStartTime(startTime);

        offday = new Offday();
        offday.setId(1L);
        offday.setDate(startTime.toLocalDate());

        employee = new Employee();
        employee.setId(1L);
        employee.setFirstName("testFirst");
        employee.setLastName("testSecond");
    }

    @Test
    void should_throw_if_input_date_before_create_date() {
        given(employeeService.findById(any())).willReturn(employee);
        given(taskService.findById(any())).willReturn(task);

        var beforeCurrDate = LocalDate.from(startTime.minusDays(2));
        var saveCardDto = new SaveTaskDailyCardDto();
        saveCardDto.setDate(beforeCurrDate);

        Assertions.assertThrows(BadTimeInputException.class, () -> service.create(saveCardDto));
    }

    @Test
    void should_throw_if_input_date_after_finish_date() {
        given(employeeService.findById(any())).willReturn(employee);
        given(taskService.findById(any())).willReturn(task);

        task.setFinishTime(startTime.plusDays(2));
        var beforeCurrDate = LocalDate.from(startTime.plusDays(5));
        var saveCardDto = new SaveTaskDailyCardDto();
        saveCardDto.setDate(beforeCurrDate);

        Assertions.assertThrows(BadTimeInputException.class, () -> service.create(saveCardDto));
    }

    @Test
    void should_throw_if_input_date_equals_offday_date() {
        given(employeeService.findById(any())).willReturn(employee);
        given(taskService.findById(any())).willReturn(task);
        employee.setOffdays(Set.of(offday));

        var saveCardDto = new SaveTaskDailyCardDto();
        saveCardDto.setDate(startTime.toLocalDate());

        Assertions.assertThrows(BadTimeInputException.class, () -> service.create(saveCardDto));
    }
}