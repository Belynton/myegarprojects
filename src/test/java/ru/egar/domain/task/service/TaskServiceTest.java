package ru.egar.domain.task.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.egar.domain.task.entity.Task;
import ru.egar.domain.task.repository.TaskRepository;
import ru.egar.exception.types.AlreadyFinishedTaskException;
import ru.egar.exception.types.BadTimeInputException;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

    @Mock
    private TaskRepository taskRepository;

    @InjectMocks
    private TaskService taskService;

    private Task task;

    @BeforeEach
    public void setup() {
        task = new Task();
        task.setId(1L);
        task.setStartTime(LocalDateTime.now());
    }

    @Test
    void should_throw_AlreadyFinishedTaskException_if_already_finished() {
        given(taskRepository.findById(1L)).willReturn(Optional.of(task));

        LocalDateTime currentTime = LocalDateTime.now();
        task.setFinishTime(currentTime);

        Assertions.assertThrows(AlreadyFinishedTaskException.class, () -> taskService.finishTask(1L, currentTime));
    }

    @Test
    void should_throw_UnableFinishTaskException_if_finished_time_in_future() {
        given(taskRepository.findById(1L)).willReturn(Optional.of(task));

        Assertions.assertThrows(BadTimeInputException.class, () -> taskService.finishTask(1L, LocalDateTime.now().plusDays(1L)));
    }
}